from db.models import Category
from db.meta import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import time

engine = create_engine('sqlite:///:memory:', echo=True)
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)

def main():
    session = Session()
    cat = Category(name="foobar")
    session.add(cat)
    session.commit()

def testing():
    session = Session()
    # Just to see different timestamp
    time.sleep(1)
    cat = Category(name="whosbar")
    session.add(cat)
    session.commit()

def dump():
    session = Session()
    for r in session.query(Category).all():
        print (f"{r.id}, {r.name}, {r.ts_created}", r.id, r.name, r.ts_created)

if __name__ == "__main__":
    main()
    testing()
    dump()
